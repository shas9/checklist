//
//  Todolist.swift
//  Checklist
//
//  Created by Kite Games Studio on 16/8/21.
//

import Foundation

class TodoList {
    var todos: [ChecklistItem] = []
    
    init() {
        for index in 0...5 {
            
            var rowItemSingle: ChecklistItem
            rowItemSingle = ChecklistItem()
            
            if index == 0 {
                rowItemSingle.text = "Hello Shahwat Hasnaine Samin"
            } else if index % 5 == 0 {
                rowItemSingle.text = "Run a marathon"
            } else if index % 5 == 1 {
                rowItemSingle.text = "Sleep"
            } else if index % 5 == 2 {
                rowItemSingle.text = "Take a jog"
            } else if index % 5 == 3 {
                rowItemSingle.text = "Study hard"
            } else if index % 5 == 4 {
                rowItemSingle.text = "This is the last of the 5 cell"
            } else {
                fatalError("This should not be executed")
            }
            
            todos.append(rowItemSingle)
            
        }
    }
    
    func newTodo() -> ChecklistItem {
        let item = ChecklistItem()
        item.text = randomTitle()
        item.checked = true
        todos.append(item)
        
        return item
    }
    
    func move(item: ChecklistItem, to index: Int) {
        guard let currentIndex = todos.firstIndex(of: item) else {
            return
        }
        
        todos.remove(at: currentIndex)
        todos.insert(item, at: index)
    }
    
    func remove(items: [ChecklistItem]) {
        for item in items {
            if let index = todos.firstIndex(of: item) {
                todos.remove(at: index)
            }
        }
    }
    
    private func randomTitle() ->String {
        let titles = ["New Item", "Generic ToDo", "Studying new todo", "Fill me out"]
        let randomNumber = Int.random(in: 0..<titles.count)
        
        return titles[randomNumber]
    }
}
