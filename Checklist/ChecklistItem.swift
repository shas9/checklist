//
//  ChecklistItem.swift
//  Checklist
//
//  Created by Kite Games Studio on 16/8/21.
//

import Foundation

class ChecklistItem: NSObject {
 
    @objc var text = ""
    var checked: Bool = false
    
    func toggleChecked() {
        checked = !checked
    }
}
