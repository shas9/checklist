//
//  ViewController.swift
//  Checklist
//
//  Created by Kite Games Studio on 16/8/21.
//

import UIKit

class TestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
}

extension TestViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath)
        
        if let label = cell.viewWithTag(1000) as? UILabel {
            if indexPath.row % 5 == 0 {
                label.text = "Run a marathon"
            } else if indexPath.row % 5 == 1 {
                label.text = "Sleep"
            } else if indexPath.row % 5 == 2 {
                label.text = "Take a jog"
            } else if indexPath.row % 5 == 3 {
                label.text = "Study hard"
            } else if indexPath.row % 5 == 4 {
                label.text = "This is the last of the 5 cell"
            } else {
                fatalError("This should not be executed")
            }
        }
        
        return cell
    }
}

extension TestViewController: UIViewControllerDelegate {
    
}

